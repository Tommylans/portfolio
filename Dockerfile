FROM node:14-alpine

RUN apk add autoconf

WORKDIR /app

COPY ./package.json ./yarn.lock /app/

RUN yarn install

COPY . /app

RUN yarn build

CMD ["yarn", "start"]
