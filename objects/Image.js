class Image {
    constructor(height, width, url, alt) {
        this.height = height;
        this.width = width;
        this.url = url;
        this.alt = alt;
    }
}

export {Image};
